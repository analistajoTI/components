package components.controller;

import java.net.URL;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;

import com.sun.javafx.scene.paint.GradientUtils.Parser;

import components.model.ItemInformation;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.SortType;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Callback;

public class WindowSearchController implements Initializable {

	@FXML
	private Label lblOption;

	@FXML
	private TextField txtSearch;

	@FXML
	private TableView<ItemInformation> tblSearch;

	@FXML
	private TableColumn<ItemInformation, String> tbcId;

	@FXML
	private TableColumn<ItemInformation, String> tbcName;

	@FXML
	private Button btnSelect;

	@FXML
	private Label lblMensaje;

	@FXML
	private Button btnClear;

	private ComboBox<ItemInformation> cmbTemporal;

	public static ObservableList<ItemInformation> tblItems = new SimpleListProperty<ItemInformation>(
			FXCollections.<ItemInformation>observableArrayList());

	private int opcion = 1;

	private int cont = 0;

	public static String myCodeTemporal = "";

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		initBtnSelect();
		initTbcId();
		initTbcDescription();
		iniTxtSearch();
		initTblSelect();
		initBtnClear();
		btnClear.setVisible(false);

	}

	public void initBtnSelect() {
		btnSelect.setOnAction(Event -> {
			if (tblSearch.getSelectionModel().getSelectedItem() != null) {
				Stage stage = (Stage) btnSelect.getScene().getWindow();
				stage.getScene().getWindow().hide();
				cmbTemporal.setValue(tblSearch.getSelectionModel().getSelectedItem());
				cmbTemporal.setDisable(false);
			} else {
				lblMensaje.setText("Seleccione un resultado en la tabla!");
			}
		});
	}

	public void initTblSelect() {
		tblSearch.setRowFactory(e -> {
			TableRow<ItemInformation> row = new TableRow<>();
			row.setOnMouseClicked(event -> {
				if (event.getClickCount() == 2 && (!row.isEmpty())) {
					if (tblSearch.getSelectionModel().getSelectedItem().getDscription() != null) {
						Stage stage = (Stage) btnSelect.getScene().getWindow();
						stage.getScene().getWindow().hide();
						cmbTemporal.setValue(tblSearch.getSelectionModel().getSelectedItem());
						cmbTemporal.setDisable(false);
					} else {
						lblMensaje.setText("Seleccione un resultado en la tabla!");
					}
				}
			});
			return row;
		});

	}

	public void fillTableView() {

		tblSearch.getItems().clear();
		tbcId.setCellValueFactory(new PropertyValueFactory<ItemInformation, String>("itemCode"));
		tbcName.setCellValueFactory(new PropertyValueFactory<ItemInformation, String>("dscription"));
		addTooltipToColumnCells(tbcId);
		addTooltipToColumnCells(tbcName);
		tbcId.setSortType(TableColumn.SortType.ASCENDING);
		tblSearch.setItems(tblItems);
		tblSearch.getSortOrder().add(tbcId);
		tblSearch.refresh();

	}

	private <T> void addTooltipToColumnCells(TableColumn column) {
		Callback<TableColumn, TableCell> existingCellFactory = column.getCellFactory();
		column.setCellFactory(c -> {
			TableCell cell = existingCellFactory.call((TableColumn) c);
			Tooltip tooltip = new Tooltip();
			tooltip.textProperty().bind(cell.itemProperty().asString());
			cell.tooltipProperty().bind(Bindings.when(Bindings.or(cell.emptyProperty(), cell.itemProperty().isNull()))
					.then((Tooltip) null).otherwise(tooltip));
			return cell;
		});
	}

	public void initTbcId() {

		tbcId.sortTypeProperty().addListener(Event -> {

			opcion = 1;
			lblOption.setText("Busqueda por ID");

		});

		tbcId.setComparator(new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				o1 = o1.replaceAll("^[a-zA-Z\\s]*$", "");
				o2 = o2.replaceAll("^[a-zA-Z\\s]*$", "");
    			Integer o1Int = Integer.parseInt(o1);
				Integer o2Int = Integer.parseInt(o2);
				return o1Int.intValue() - o2Int.intValue();
			}
		});
	}

	public void initTbcDescription() {
		tbcName.sortTypeProperty().addListener(Event -> {

			opcion = 2;
			lblOption.setText("Busqueda por Descripcion");

		});
	}

	public void initBtnClear() {
		btnClear.setOnAction(Event -> {
			txtSearch.setText("");
		});
	}

	public void iniTxtSearch() {
		txtSearch.textProperty().addListener((obj, oldVal, newVal) -> {
			if (opcion == 1) {
				ObservableList<ItemInformation> tbltemp = tblItems;
				ObservableList<ItemInformation> tbltemp1 = new SimpleListProperty<ItemInformation>(
						FXCollections.<ItemInformation>observableArrayList());
				if (!txtSearch.getText().equals("") && txtSearch.getText() != null) {
					btnClear.setVisible(true);
					for (ItemInformation itemInformation : tbltemp) {
						if (itemInformation.getItemCode().toLowerCase().startsWith(txtSearch.getText().toLowerCase())) {
							tbltemp1.add(itemInformation);
						}
					}

					tbcId.setSortType(TableColumn.SortType.ASCENDING);
					tblSearch.setItems(tbltemp1);
					tblSearch.getSortOrder().add(tbcId);
					tblSearch.refresh();

				} else {
					tbcId.setSortType(TableColumn.SortType.ASCENDING);
					tblSearch.setItems(tbltemp);
					tblSearch.getSortOrder().add(tbcId);
					tblSearch.refresh();
					btnClear.setVisible(false);
				}

			} else {
				ObservableList<ItemInformation> tbltemp = tblItems;
				ObservableList<ItemInformation> tbltemp1 = new SimpleListProperty<ItemInformation>(
						FXCollections.<ItemInformation>observableArrayList());
				if (!txtSearch.getText().equals("") && txtSearch.getText() != null) {
					btnClear.setVisible(true);
					for (ItemInformation itemInformation : tbltemp) {
						if (itemInformation.getDscription().toLowerCase()
								.startsWith(txtSearch.getText().toLowerCase())) {
							tbltemp1.add(itemInformation);
						}
					}
					tbcName.setSortType(TableColumn.SortType.ASCENDING);
					tblSearch.setItems(tbltemp1);
					tblSearch.getSortOrder().add(tbcName);
					tblSearch.refresh();
				} else {
					tbcName.setSortType(TableColumn.SortType.ASCENDING);
					tblSearch.setItems(tbltemp);
					tblSearch.getSortOrder().add(tbcName);
					tblSearch.refresh();
					btnClear.setVisible(false);
				}
			}
		});

	}

	public ComboBox<ItemInformation> getCmbTemporal() {
		return cmbTemporal;
	}

	public void setCmbTemporal(ComboBox<ItemInformation> cmbTemporal) {
		this.cmbTemporal = cmbTemporal;
	}

}

