package components.controller;

import components.model.ItemInformation;
import javafx.application.Platform;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

public class SearchComponent extends AnchorPane {

	private ComboBox<ItemInformation> myComboBox = new ComboBox();
	private Button btnSearch = new Button();
	private Button btnClear  = new Button();
	private ItemInformation valor = new ItemInformation();
	private String myCode = "";

	public SearchComponent() {
		super();
		this.setPrefHeight(24.0);
		this.setPrefWidth(322.0);

		setLeftAnchor(myComboBox, 0.0);
		setRightAnchor(myComboBox, 0.0);
		setTopAnchor(myComboBox, 0.0);
		setBottomAnchor(myComboBox, 0.0);
		myComboBox.setPrefWidth(200.0);

		setBottomAnchor(btnClear, 4.0);
		setRightAnchor(btnClear, 28.0);
		setTopAnchor(btnClear, 4.0);
		btnClear.setVisible(false);
		btnClear.setGraphicTextGap(0.0);
		btnClear.setMinHeight(10);
		btnClear.setMinWidth(13);
		btnClear.setMnemonicParsing(false);
		btnClear.setPrefHeight(20);
		btnClear.setPrefWidth(22);
		btnClear.setText("c");
		btnClear.setTextFill(Color.WHITE);

		setBottomAnchor(btnSearch, 4.0);
		setRightAnchor(btnSearch, 4.0);
		setTopAnchor(btnSearch, 4.0);
		btnSearch.setGraphicTextGap(0.0);
		btnSearch.setMinHeight(10);
		btnSearch.setMinWidth(13);
		btnSearch.setMnemonicParsing(false);
		btnSearch.setPrefHeight(20);
		btnSearch.setPrefWidth(22);
		btnSearch.setText("x");
		btnSearch.setTextFill(Color.WHITE);

		this.getChildren().addAll(myComboBox, btnSearch, btnClear);
	}

	public void fillCmb(ObservableList<ItemInformation> list) {
		myComboBox.setOnMouseClicked(EventHandler -> {
			if (!(myComboBox.getValue() == null ? "" : myComboBox.getValue()).equals("") && !myComboBox.getValue().equals("")) {
				valor = myComboBox.getValue();
				btnClear.setVisible(true);
			}else {
				btnClear.setVisible(false);
			}
			myComboBox.getItems().clear();
			if (!valor.equals("")) {
				myComboBox.setValue(valor);
			}
		});
		

		myComboBox.setItems(list);
		new AutoCompleteBox(myComboBox, btnClear);
	}
	
	public void initBtnClear() {
		btnClear.setOnAction(Event->{
			myComboBox.setValue(new ItemInformation());
			btnClear.setVisible(false);
		});
	}

	public void initCmbSearch(ObservableList<ItemInformation> list) {
		btnSearch.setOnAction(event -> {
			myComboBox.setDisable(true);
			Window window = (((Node) event.getSource()).getScene()).getWindow();
			
			try {
				
				FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/WindowSearch.fxml"));
				Parent root =(Parent) loader.load();
				
				Scene scene = new Scene(root);
				Stage stage = new Stage();
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(scene);
				stage.setOnCloseRequest(e -> {
					myComboBox.setDisable(false);
				});
				stage.show();
				
				WindowSearchController controller = loader.<WindowSearchController>getController();
				setMyCode(controller.myCodeTemporal);
				controller.setCmbTemporal(myComboBox);
				controller.tblItems = list;
				controller.fillTableView();

			} catch (Exception e) {
			}
		});
	}


	public ComboBox<ItemInformation> getMyComboBox() {
		return myComboBox;
	}

	public void setMyComboBox(ComboBox<ItemInformation> myComboBox) {
		this.myComboBox = myComboBox;
	}

	public String getMyCode() {
		return myCode;
	}

	public void setMyCode(String myCode) {
		this.myCode = myCode;
	}
	
	
	

}