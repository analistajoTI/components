package components.controller;

import java.util.stream.Stream;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import com.sun.javafx.scene.control.skin.ComboBoxListViewSkin;

import components.model.ItemInformation;
import javafx.util.StringConverter;


public class AutoCompleteBox {
	private ComboBox<ItemInformation> cmb;
	private Button btnClear;
	ItemInformation filter = new ItemInformation();
	private ObservableList<ItemInformation> originalItems;
	private ComboBoxListViewSkin<ItemInformation> cbSkin;
	private String letra = "";

	public AutoCompleteBox(ComboBox<ItemInformation> cmb, Button btnClear) {
		this.cmb = cmb;
		cbSkin = new ComboBoxListViewSkin<ItemInformation>(cmb);
		this.btnClear = btnClear;
		originalItems = FXCollections.observableArrayList(cmb.getItems());
		cmb.setOnKeyPressed(this::handleOnKeyPressed);
		cmb.setOnHidden(this::handleOnHiding);

		cbSkin.getPopupContent().addEventFilter(KeyEvent.KEY_PRESSED, (event) -> {
			if (event.getCode() == KeyCode.SPACE) {
				letra = filter.getDscription();
				filter.setDscription(letra + " ");
				event.consume();
			}
	
		});

		cmb.setConverter(new StringConverter<ItemInformation>() {
			@Override
			public String toString(ItemInformation object) {
				// TODO Auto-generated method stub
				String result = "";

				if (object != null) {
					if (object.getDscription() != null) {
						result = object.getDscription().toUpperCase();
					}

				}

				return result;
			}

			@Override
			public ItemInformation fromString(String string) {
				// TODO Auto-generated method stub
				return null;
			}

		});
	}

	public void handleOnKeyPressed(KeyEvent e) {
		ObservableList<ItemInformation> filteredList = FXCollections.observableArrayList();
		KeyCode code = e.getCode();
		if (code.isLetterKey() || code.isDigitKey()) {

			if (filter.getDscription() != null) {
				letra = filter.getDscription();
				filter.setDscription(letra + e.getCode());
			}
		}

		if (code == KeyCode.BACK_SPACE && filter.getDscription().length() > 0) {
			filter.setDscription((filter.getDscription()).substring(0, filter.getDscription().length() - 1));
		}
		if (code.equals(KeyCode.ESCAPE)) {
			filter.setDscription("");
		}
		if (filter.getDscription() != null) {
			if (filter.getDscription().length() > 2) {
				cmb.setValue(filter);
				Stream<ItemInformation> itens = originalItems.stream();
				String txtUsr = filter.getDscription().toString().toLowerCase();
				itens.filter(el -> el.getDscription().toString().toLowerCase().startsWith(txtUsr))
						.forEach(filteredList::add);
				cmb.getItems().setAll(filteredList);
				cmb.setValue(new ItemInformation(filter.getItemCode(), filter.getDscription()));
				cmb.show();
			} else {
				cmb.setValue(new ItemInformation(filter.getItemCode(), filter.getDscription()));
				cmb.show();

			}
		}
	}

	public void handleOnHiding(Event e) {
		filter.setDscription("");
		ItemInformation s = cmb.getSelectionModel().getSelectedItem();
			cmb.getItems().setAll(originalItems);
			if(cmb != null) {
			cmb.getSelectionModel().select(s);
			}
			if (!originalItems.contains(cmb.getValue())) {
				cmb.setValue(new ItemInformation());
			} else {
				btnClear.setVisible(true);
			}
				
	}

}
