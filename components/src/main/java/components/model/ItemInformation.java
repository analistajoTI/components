package components.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ItemInformation {
	private StringProperty itemCode = new SimpleStringProperty();
	private StringProperty dscription = new SimpleStringProperty();


	public ItemInformation() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ItemInformation(String itemCode, String dscription) {
		this.itemCode = new SimpleStringProperty(itemCode);
		this.dscription = new SimpleStringProperty(dscription);
	}

	public final StringProperty itemCodeProperty() {
		return this.itemCode;
	}

	public String getItemCode() {
		return this.itemCodeProperty().get();
	}

	public void setItemCode(String firstName) {
		this.itemCodeProperty().set(firstName);
	}

	public final StringProperty dscriptionProperty() {
		return this.dscription;
	}

	public String getDscription() {
		return this.dscriptionProperty().get();
	}

	public void setDscription(String dscription) {
		this.dscriptionProperty().set(dscription);
	}

	@Override
	public String toString() {
		return "ItemInformation [itemCode=" + itemCode + ", dscription=" + dscription+"]";
	}
	}